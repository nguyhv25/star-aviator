//COLORS
var Colors = {
  red: 0xf25346,
  white: 0xd8d0d1,
  pink: 0xf5986e,
  brown: 0x59332e,
  brownDark: 0x23190f,
  blue: 0x299c14,
};

// THREEJS RELATED VARIABLES

var scene,
  camera,
  fieldOfView,
  aspectRatio,
  nearPlane,
  farPlane,
  renderer,
  container;

//SCREEN VARIABLES

var ennemiesPool = [];
var lasersPool = [];

var HEIGHT, WIDTH;

//INIT THREE JS, SCREEN AND MOUSE EVENTS

function createScene() {
  HEIGHT = window.innerHeight;
  WIDTH = window.innerWidth;

  scene = new THREE.Scene();
  aspectRatio = WIDTH / HEIGHT;
  fieldOfView = 60;
  nearPlane = 1;
  farPlane = 10000;
  camera = new THREE.PerspectiveCamera(
    fieldOfView,
    aspectRatio,
    nearPlane,
    farPlane
  );
  scene.fog = new THREE.Fog(0xf7d9aa, 100, 950);
  camera.position.x = -200;
  camera.position.z = 0;
  camera.position.y = 50;
  camera.up = new THREE.Vector3(0, 1, 0);
  camera.lookAt(new THREE.Vector3(0, 0, 0));

  renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
  renderer.setSize(WIDTH, HEIGHT);
  renderer.shadowMap.enabled = true;
  container = document.getElementById("world");
  container.appendChild(renderer.domElement);

  window.addEventListener("resize", handleWindowResize, false);
}

// HANDLE SCREEN EVENTS

function handleWindowResize() {
  HEIGHT = window.innerHeight;
  WIDTH = window.innerWidth;
  renderer.setSize(WIDTH, HEIGHT);
  camera.aspect = WIDTH / HEIGHT;
  camera.updateProjectionMatrix();
}

// LIGHTS

var ambientLight, hemisphereLight, shadowLight;

function createLights() {
  hemisphereLight = new THREE.HemisphereLight(0xaaaaaa, 0x000000, 0.9);

  ambientLight = new THREE.AmbientLight(0xdc8874, 0.5);

  shadowLight = new THREE.DirectionalLight(0xffffff, 0.9);
  shadowLight.position.set(150, 350, 350);
  shadowLight.castShadow = true;
  shadowLight.shadow.camera.left = -400;
  shadowLight.shadow.camera.right = 400;
  shadowLight.shadow.camera.top = 400;
  shadowLight.shadow.camera.bottom = -400;
  shadowLight.shadow.camera.near = 1;
  shadowLight.shadow.camera.far = 1000;
  shadowLight.shadow.mapSize.width = 2048;
  shadowLight.shadow.mapSize.height = 2048;

  scene.add(hemisphereLight);
  scene.add(shadowLight);
  scene.add(ambientLight);
}

Sky = function () {
  this.mesh = new THREE.Object3D();
  this.nClouds = 50;
  this.clouds = [];
  var stepAngle = (Math.PI * 2) / this.nClouds;
  for (var i = 0; i < this.nClouds; i++) {
    var c = new Cloud();
    this.clouds.push(c);
    var a = stepAngle * i;
    var h = 750 + Math.random() * 200;
    c.mesh.position.y = Math.sin(a) * h;
    c.mesh.position.x = Math.cos(a) * h;
    c.mesh.position.z = 500 - Math.random() * 1000;
    c.mesh.rotation.z = a + Math.PI / 2;
    var s = 1 + Math.random() * 2;
    c.mesh.scale.set(s, s, s);
    this.mesh.add(c.mesh);
  }
};

Sea = function () {
  var geom = new THREE.CylinderGeometry(500, 500, 800, 40, 10);
  geom.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));
  geom.mergeVertices();
  var l = geom.vertices.length;

  this.waves = [];

  for (var i = 0; i < l; i++) {
    var v = geom.vertices[i];
    this.waves.push({
      y: v.y,
      x: v.x,
      z: v.z,
      ang: Math.random() * Math.PI * 2,
      amp: 5 + Math.random() * 15,
      speed: 0.016 + Math.random() * 0.032,
    });
  }
  var mat = new THREE.MeshPhongMaterial({
    color: Colors.blue,
    transparent: true,
    opacity: 1,
    shading: THREE.FlatShading,
  });

  this.mesh = new THREE.Mesh(geom, mat);
  this.mesh.receiveShadow = true;
};

Sea.prototype.moveWaves = function () {
  var verts = this.mesh.geometry.vertices;
  var l = verts.length;
  for (var i = 0; i < l; i++) {
    var v = verts[i];
    var vprops = this.waves[i];
    v.x = vprops.x + Math.cos(vprops.ang) * vprops.amp;
    v.y = vprops.y + Math.sin(vprops.ang) * vprops.amp;
    vprops.ang += vprops.speed;
  }
  this.mesh.geometry.verticesNeedUpdate = true;
  sea.mesh.rotation.z += 0.005;
};

Cloud = function () {
  this.mesh = new THREE.Object3D();
  this.mesh.name = "cloud";
  var geom = new THREE.CubeGeometry(20, 20, 20);
  var mat = new THREE.MeshPhongMaterial({
    color: Colors.white,
  });

  var nBlocs = 3 + Math.floor(Math.random() * 3);
  for (var i = 0; i < nBlocs; i++) {
    var m = new THREE.Mesh(geom.clone(), mat);
    m.position.x = i * 15;
    m.position.y = Math.random() * 10;
    m.position.z = Math.random() * 10;
    m.rotation.z = Math.random() * Math.PI * 2;
    m.rotation.y = Math.random() * Math.PI * 2;
    var s = 0.1 + Math.random() * 0.9;
    m.scale.set(s, s, s);
    m.castShadow = true;
    m.receiveShadow = true;
    this.mesh.add(m);
  }
};

// 3D Models
var sea;
var airplane;
var arwing;

async function createPlane() {
  const loader = new THREE.GLTFLoader();
  loader.load(
    "../assets/arwing_assault/scene.gltf",
    function (gltf) {
      arwing = gltf.scene;
      arwing.scale.set(0.05, 0.05, 0.05);
      scene.add(arwing);
    },
    undefined,
    undefined
  );
}

function createSea() {
  sea = new Sea();
  sea.mesh.position.y = -600;
  scene.add(sea.mesh);
}

function createSky() {
  sky = new Sky();
  sky.mesh.position.y = -600;
  scene.add(sky.mesh);
}

function loop() {
  updatePlane();
  updateCameraFov();
  updateLasers();
  sea.moveWaves();
  sky.mesh.rotation.z += 0.01;

  if (facemeshModel && videoDataLoaded) {
    // model and video both loaded
    facemeshModel.pipeline.maxFaces = MAX_FACES;
    facemeshModel.estimateFaces(capture).then(function (_faces) {
      // we're faceling an async promise
      // best to avoid drawing something here! it might produce weird results due to racing

      myFaces = _faces.map((x) => packFace(x, VTX)); // update the global myFaces object with the detected faces

      if (!myFaces.length) {
        // haven't found any faces
        statusText = "Show some faces!";
      } else {
        // display the confidence, to 3 decimal places
        statusText =
          "Confidence: " +
          Math.round(myFaces[0].faceInViewConfidence * 1000) / 1000;
        // console.log(
        //   myFaces[0].scaledMesh[38][0] - myFaces[0].scaledMesh[40][0]
        // );
        console.log(
          myFaces[0].scaledMesh[159][1] - myFaces[0].scaledMesh[145][1]
        );
      }
      tex.drawImage(capture, 0, 0);
      updateMeshes(myFaces);
    });
  }

  dbg.clearRect(0, 0, dbg.canvas.width, dbg.canvas.height);

  dbg.save();
  dbg.fillStyle = "red";
  dbg.strokeStyle = "red";
  dbg.scale(0.5, 0.5); //halfsize;

  dbg.drawImage(capture, 0, 0);
  drawFaces(myFaces);
  dbg.restore();

  dbg.save();
  dbg.fillStyle = "red";
  dbg.fillText(statusText, 2, 60);
  dbg.restore();

  renderer.render(scene, camera);
  requestAnimationFrame(loop);
}

function updatePlane() {
  var targetY = normalize(mousePos.y, -1, 1, -80, 100);
  var targetX = normalize(mousePos.x, -1, 1, -100, 100);

  if (arwing) {
    arwing.position.z += (targetX - arwing.position.z) * 0.1;
    arwing.position.y += (targetY - arwing.position.y) * 0.1;

    arwing.rotation.z = (arwing.position.y - targetY) * 0.0064;
    arwing.rotation.x = (targetX - arwing.position.z) * 0.0128;
    arwing.rotation.y = 3.14;
  }
}

function updateCameraFov() {
  camera.fov = normalize(0, -1, 1, 40, 80);
  camera.updateProjectionMatrix();
}

function normalize(v, vmin, vmax, tmin, tmax) {
  var nv = Math.max(Math.min(v, vmax), vmin);
  var dv = vmax - vmin;
  var pc = (nv - vmin) / dv;
  var dt = tmax - tmin;
  var tv = tmin + pc * dt;
  return tv;
}

async function init(event) {
  document.addEventListener("mousemove", handleMouseMove, false);
  createScene();
  createLights();
  await createPlane();
  createSea();
  createSky();
  loop();
}

// HANDLE MOUSE EVENTS

var mousePos = { x: 0, y: 0 };

function handleMouseMove(event) {
  var tx = -1 + (event.clientX / WIDTH) * 2;
  var ty = 1 - (event.clientY / HEIGHT) * 2;
  mousePos = { x: tx, y: ty };
}

Ennemy = function () {
  var geom = new THREE.TetrahedronGeometry(8, 2);
  var mat = new THREE.MeshPhongMaterial({
    color: Colors.red,
    shininess: 0,
    specular: 0xffffff,
    shading: THREE.FlatShading,
  });
  this.mesh = new THREE.Mesh(geom, mat);
  this.mesh.castShadow = true;
  this.angle = 0;
  this.dist = 0;
};

EnnemiesHolder = function () {
  this.mesh = new THREE.Object3D();
  this.ennemiesInUse = [];
};

EnnemiesHolder.prototype.spawnEnnemies = function () {};

function createEnnemies() {
  for (var i = 0; i < 10; i++) {
    var ennemy = new Ennemy();
    ennemiesPool.push(ennemy);
  }
  ennemiesHolder = new EnnemiesHolder();
  scene.add(ennemiesHolder.mesh);
}

function shootLaser() {
  const geometry = new THREE.CylinderGeometry(2, 2, 50, 32);
  const material = new THREE.MeshPhongMaterial({
    color: 0xfa0000,
    shininess: 0,
    specular: 0xffffff,
    shading: THREE.FlatShading,
  });
  const cylinder = new THREE.Mesh(geometry, material);
  scene.add(cylinder);
  var arwing_position = arwing.position;
  cylinder.position.x = 10;
  cylinder.position.y = arwing_position.y;
  cylinder.position.z = arwing_position.z;
  cylinder.rotation.z = 3.14 / 2;
  lasersPool.push(cylinder);
}

function updateLasers() {
  for (var i = lasersPool.length - 1; i >= 0; i--) {
    if (lasersPool[i].position.x > 1000) {
      scene.remove(lasersPool[i]);
      lasersPool.splice(i, 1);
    }
  }

  for (var i = 0; i < lasersPool.length; i++) {
    lasersPool[i].position.x += 50;
  }
}

window.addEventListener("load", init, false);
window.addEventListener("mousedown", shootLaser);

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

const TEXTURE_MODES = [
  "normal",
  "video",
  "shaded",
  "texture",
  "texture-shaded",
];
const NORMAL_MODES = ["vertex", "face"];
const KEYPOINT_MODES = {
  VTX7,
  VTX33,
  VTX68,
  VTX468,
};

var TEXTURE_MODE = "texture";
var NORMAL_MODE = "vertex";
var VTX = KEYPOINT_MODES["VTX468"];

// select the right triangulation based on vertices
var TRI =
  VTX == VTX7 ? TRI7 : VTX == VTX33 ? TRI33 : VTX == VTX68 ? TRI68 : TRI468;
var UV = VTX == VTX7 ? UV7 : VTX == VTX33 ? UV33 : VTX == VTX68 ? UV68 : UV468;

var MAX_FACES = 1; //default 10

var facemeshModel = null; // this will be loaded with the facemesh model

var videoDataLoaded = false; // is webcam capture ready?

var statusText = "Loading facemesh model...";

var myFaces = []; // faces detected in this browser
// currently facemesh only supports single face, so this will be either empty or singleton

var faceMeshes = []; // these are threejs objects that makes up the rendering of the faces
// stored as { userId : Array<Object3D> }

// html canvas for drawing debug view
var dbg = document.createElement("canvas").getContext("2d");
dbg.canvas.style.position = "absolute";
dbg.canvas.style.left = "0px";
dbg.canvas.style.top = "0px";
dbg.canvas.style.zIndex = 100; // "bring to front"
document.body.appendChild(dbg.canvas);

var tex = document.createElement("canvas").getContext("2d");
tex.canvas.style.position = "absolute";
tex.canvas.style.left = "0px";
tex.canvas.style.top = "0px";
tex.canvas.style.zIndex = -100; // "send to back"
tex.canvas.style.pointerEvents = "none";
tex.canvas.style.opacity = 0;
document.body.appendChild(tex.canvas);

// read video from webcam
var capture = document.createElement("video");
capture.playsinline = "playsinline";
capture.autoplay = "autoplay";
navigator.mediaDevices
  .getUserMedia({ audio: false, video: { facingMode: "user" } })
  .then(function (stream) {
    window.stream = stream;
    capture.srcObject = stream;
  });

// hide the video element
capture.style.position = "absolute";
capture.style.opacity = 0;
capture.style.zIndex = -100; // "send to back"

// signal when capture is ready and set size for debug canvas
capture.onloadeddata = function () {
  console.log("video initialized");
  videoDataLoaded = true;
  dbg.canvas.width = capture.videoWidth / 2; // half size
  dbg.canvas.height = capture.videoHeight / 2;

  tex.canvas.width = capture.videoWidth; // half size
  tex.canvas.height = capture.videoHeight;
};

var texture = new THREE.TextureLoader().load("assets/test.png");
var material = new THREE.MeshBasicMaterial({ map: texture });

// update threejs object position and orientation according to data sent from server
// threejs has a "scene" model, so we don't have to specify what to draw each frame,
// instead we put objects at right positions and threejs renders them all
function updateMeshes(faces) {
  var imgData;

  while (faceMeshes.length < faces.length) {
    const geom = new THREE.Geometry();
    for (var i = 0; i < 468; i++) {
      geom.vertices.push(new THREE.Vector3(i, 0, 0));
    }
    for (var i = 0; i < TRI.length; i += 3) {
      geom.faces.push(new THREE.Face3(TRI[i], TRI[i + 1], TRI[i + 2]));

      if (TEXTURE_MODE == "texture" || TEXTURE_MODE == "texture-shaded") {
        geom.faceVertexUvs[0].push([
          new THREE.Vector2(UV[TRI[i]][0], 1 - UV[TRI[i]][1]),
          new THREE.Vector2(UV[TRI[i + 1]][0], 1 - UV[TRI[i + 1]][1]),
          new THREE.Vector2(UV[TRI[i + 2]][0], 1 - UV[TRI[i + 2]][1]),
        ]);
      }
    }
    var mesh = new THREE.Mesh(geom, material);
    mesh.rotation.y = -3.14 / 2;
    mesh.rotation.z -= 3.14 / 8;
    mesh.position.y = 80;
    mesh.position.x = 200;
    mesh.position.z = 0;
    scene.add(mesh);
    faceMeshes.push(mesh);
  }
  // next, we remove the extra meshes
  while (faceMeshes.length > faces.length) {
    scene.remove(faceMeshes.pop());
  }

  for (var i = 0; i < faceMeshes.length; i++) {
    let geom = faceMeshes[i].geometry;
    let targ = faces[i].scaledMesh;
    for (var j = 0; j < targ.length; j++) {
      var p = webcam2space(...targ[j]);
      geom.vertices[j].set(p.x, p.y, p.z);
    }

    for (var j = 0; j < TRI.length; j += 3) {
      if (!geom.faceVertexUvs[0][j / 3]) {
        geom.faceVertexUvs[0][j / 3] = [
          new THREE.Vector2(),
          new THREE.Vector2(),
          new THREE.Vector2(),
        ];
      }
      if (TEXTURE_MODE == "video") {
        geom.faceVertexUvs[0][j / 3][0].x = targ[TRI[j]][0] / tex.canvas.width;
        geom.faceVertexUvs[0][j / 3][0].y =
          1 - targ[TRI[j]][1] / tex.canvas.height;
        geom.faceVertexUvs[0][j / 3][1].x =
          targ[TRI[j + 1]][0] / tex.canvas.width;
        geom.faceVertexUvs[0][j / 3][1].y =
          1 - targ[TRI[j + 1]][1] / tex.canvas.height;
        geom.faceVertexUvs[0][j / 3][2].x =
          targ[TRI[j + 2]][0] / tex.canvas.width;
        geom.faceVertexUvs[0][j / 3][2].y =
          1 - targ[TRI[j + 2]][1] / tex.canvas.height;
      }
    }
    if (NORMAL_MODE == "vertex") {
      geom.computeVertexNormals();
    } else if (NORMAL_MODE == "face") {
      geom.computeFaceNormals();
    }
    geom.verticesNeedUpdate = true;
  }
}

// Load the MediaPipe facemesh model assets.
facemesh.load().then(function (_model) {
  console.log("model initialized.");
  statusText = "Model loaded.";
  facemeshModel = _model;
});

// draw a face object (2D debug view) returned by facemesh
function drawFaces(faces, noKeypoints) {
  for (var i = 0; i < faces.length; i++) {
    const keypoints = faces[i].scaledMesh;

    for (var j = 0; j < TRI.length; j += 3) {
      var a = keypoints[TRI[j]];
      var b = keypoints[TRI[j + 1]];
      var c = keypoints[TRI[j + 2]];

      dbg.beginPath();
      dbg.moveTo(a[0], a[1]);
      dbg.lineTo(b[0], b[1]);
      dbg.lineTo(c[0], c[1]);
      dbg.closePath();
      dbg.stroke();
    }
  }
}

// hash to a unique color for each user ID
function uuid2color(uuid) {
  var col = 1;
  for (var i = 0; i < uuid.length; i++) {
    var cc = uuid.charCodeAt(i);
    col = (col * cc) % 0xffffff;
  }
  return [(col >> 16) & 0xff, (col >> 8) & 0xff, col & 0xff];
}

// transform webcam coordinates to threejs 3d coordinates
function webcam2space(x, y, z) {
  return new THREE.Vector3(
    x - capture.videoWidth / 2,
    -(y - capture.videoHeight / 2), // in threejs, +y is up
    -z
  );
}

// reduce vertices to the desired set, and compress data as well
function packFace(face, set) {
  var ret = {
    scaledMesh: [],
  };
  for (var i = 0; i < set.length; i++) {
    var j = set[i];
    ret.scaledMesh[i] = [
      Math.floor(face.scaledMesh[j][0] * 100) / 100,
      Math.floor(face.scaledMesh[j][1] * 100) / 100,
      Math.floor(face.scaledMesh[j][2] * 100) / 100,
    ];
  }
  return ret;
}
